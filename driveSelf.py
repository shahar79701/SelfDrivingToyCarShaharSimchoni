import tensorflow as tf
import pickle
import numpy as np
import motor as MotorControl
from picamera.array import PiRGBArray
from picamera import PiCamera
import time 
import makeData
import cv2

motor = MotorControl.Motor(11, 15, 16, 18)

n_nodes_hl1 = 70
n_classes = 4

batch_size = 50
hm_epochs = 10

picSize = 33280 # enter pic size
x = tf.placeholder('float', [None, picSize])
y = tf.placeholder('float')


current_epoch = tf.Variable(1)

hidden_1_layer = {'f_fum':n_nodes_hl1,
                  'weight':tf.Variable(tf.random_normal([picSize, n_nodes_hl1])),
                  'bias':tf.Variable(tf.random_normal([n_nodes_hl1]))}

output_layer = {'f_fum':None,
                'weight':tf.Variable(tf.random_normal([n_nodes_hl1, n_classes])),
                'bias':tf.Variable(tf.random_normal([n_classes])),}

    
def neural_network_model(data):
	
    l1 = tf.add(tf.matmul(data,hidden_1_layer['weight']), hidden_1_layer['bias'])
    l1 = tf.nn.relu(l1)

    output = tf.matmul(l1,output_layer['weight']) + output_layer['bias']

    return output

saver = tf.train.Saver()

def use_neural_network():
    prediction = neural_network_model(x)
        
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        #saver.restore(sess,"model.ckpt.meta.tmpcffc0592360f4f3291d8a9d9098e8441")
		
        camera = PiCamera()
        camera.resolution = (208, 160)
        camera.framerate = 20
        rawCapture = PiRGBArray(camera, size=(208,160))
        
        
        while True:
                camera.capture(rawCapture, 'bgr', use_video_port=True)
                frame = rawCapture.array
                motor.stop()
                features = makeData.frameFilterToList(frame)
                print(features[0][0])
                #features = makeData.dataPreper2L(features)
                a = []
                rawCapture.truncate(0)
                for z in features:
                    for y in z:
                        if y == 0:
                            a.append(0)
                        else:
                            a.append(1)
                print(len(a))
                result = (sess.run(tf.argmax(prediction.eval(feed_dict={x:[a]}),1)))
                print(result)
                #result = list(result).index('1')
                print(result)
                '''if result == 0:
                        motor.backward()
                elif result == 1:
                        motor.forward_right()
                elif result == 2:
                        motor.forward_left()
                elif result == 3:
                        motor.forward()'''
                        
                #print (result)
                
use_neural_network()