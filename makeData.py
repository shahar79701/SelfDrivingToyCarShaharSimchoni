import numpy as np
import random
import glob
import os
import cv2
from time import sleep
from datetime import datetime
import cPickle as pickle

pathREW = '/home/pi/DataSet/'
path = '/home/pi/TF-DataSet/'


def makeData():
    featuresList = []
    for log in glob.glob(os.path.join(pathREW, '*.txt')):
        fileP,dataTime,format = log.split('*') #  name - data time - format
        #video = pathREW+'driveFILTER*'+dataTime+'*.avi'
        video = pathREW+'driveFILTER*'+dataTime+'*.avi'
        cap = cv2.VideoCapture(video)
        
        width  = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        
        print(video)
        print(log)
        
        
        
        with open(log) as l:
            for i, line in enumerate(l):
                if ('n' in line):
                    continue 
                commend = []
                
                for c in line:
                    if c != '\n':
                        commend.append(c)
                ret, frame = cap.read()                
                features =frameFilterToList(frame)
                featuresList.append([features,commend])
                
        print('done for ' + video + ' files')
    
    random.shuffle(featuresList)
    featuresList = np.array(featuresList)
    tasting_size = int(0.1*len(featuresList))
    
    train_x = list(featuresList[:,0][:-tasting_size])
    train_y = list(featuresList[:,1][:-tasting_size])
    test_x = list(featuresList[:,0][-tasting_size:])
    test_y = list(featuresList[:,1][-tasting_size:])
    
    train_x, test_x= helper(train_x,test_x)
    return train_x, train_y, test_x, test_y

def frameFilterToList(frame):
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_red = np.array([30,150,50])
    upper_red = np.array([255,255,180])
    mask = cv2.inRange(hsv, lower_red, upper_red)
    (thresh, im_bw) = cv2.threshold(mask, 1, 1, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    thresh = 127
    im_bw = cv2.threshold(mask, thresh, 255, cv2.THRESH_BINARY)[1]
    return list(im_bw)


def writeToFile():
    x1 ,y1 ,x2, y2 = makeData()
    fx1 = open(path +' -trainF.txt','w')
    fy1 = open(path +' -trainL.txxt','w')
    fx2 = open(path +' -testF.txxt','w')
    fy2 = open(path +' -testL.txxt','w')
    for i in x1:
        fx1.write('%s\n'%i)
    for i in y1:
        fy1.write('%sn\n'%i)
    for i in x2:
        fx2.write('%sn\n'%i)
    for i in y2:
        fy2.write('%sn\n'%i)
        
def readFromFile():
    fx1 = path +' -trainF.txt'
    fy1 = path +' -trainL.txxt'
    fx2 = path +' -testF.txxt'
    fy2 =path +' -testL.txxt'
    x1 = []
    with open(fx1) as l:
            for i, line in enumerate(l):
                line = line.replace('\n','')
                line = line.replace(' ','')
                line = line.replace('[','')
                line = line.replace(']','')
                line = line.replace("'",'')
                x1.append(line.split(','))
    y1 = []
    with open(fy1) as l:
            for i, line in enumerate(l):
                line = line.replace('\n','')
                line = line.replace(' ','')
                line = line.replace('[','')
                line = line.replace(']','')
                line = line.replace("'",'')
                y1.append(line.split(','))
    x2 = []
    with open(fx2) as l:
            for i, line in enumerate(l):
                line = line.replace('\n','')
                line = line.replace(' ','')
                line = line.replace('[','')
                line = line.replace(']','')
                line = line.replace("'",'')
                x2.append(line.split(','))
    y2 = []
    with open(fy2) as l:
            for i, line in enumerate(l):
                line = line.replace('\n','')
                line = line.replace(' ','')
                line = line.replace('[','')
                line = line.replace(']','')
                line = line.replace("'",'')
                y2.append(line.split(','))
                
    return x1, y1, x2, y2
    

def helper(x1,x2):
    x1_new = []
    x2_new = []
    for i in x1:
        temp = []
        for x in i:
            for y in x:
                if y == 0:
                    temp.append('0')
                else:
                    temp.append('1')
        x1_new.append(temp)
    for i in x1:
        temp = []
        for x in i:
            for y in x:
                if y == 0:
                    temp.append('0')
                else:
                    temp.append('1')
        x2_new.append(temp)
    return x1_new,x2_new
            
def dataPreper3L(x1):
    x1_new = []
    #print(x1[0][0][0])
    for i in x1:
        temp = []
        for x in i:
            for y in x:
                if y == '0':
                    temp.append(0)
                else:
                    temp.append(1)
        x1_new.append(temp)
    return x1_new
    
def dataPreper2L(x1):
    x1_new = []
    #print(x1)
    
    for i in x1:
        temp = []
        for x in i:
            if x == '0':
                temp.append(0)
            else:
                temp.append(1)
        x1_new.append(temp)
    return x1_new
    
    
def getData():
    with open(path+'DataSet.pickle','rb') as file:
        l = pickle.load(file)
    return l[0],l[1],l[2],l[3]

'''
timestamp = datetime.now()
x1, y1, x2, y2 = makeData()
with open(path+'DataSet.pickle','wb') as file:
    pickle.dump([x1, y1, x2, y2],file)
#writeToFile()
#helper()
print('kk  ' + str(datetime.now() - timestamp  )) #8 min to 216 frames on the rasbery
print('  for -' + str(len(x1)+len(x2)))
'''