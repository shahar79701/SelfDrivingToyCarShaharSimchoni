import argparse
import RPi.GPIO as GPIO
import tornado.ioloop, tornado.web
from datetime import datetime
import os
#import sys
from operator import itemgetter
import gen
import requests
from time import sleep

import motor as MotorControl
import videoStreem
#import vieoSaveFileANDfiler

# global vars
motor = MotorControl.Motor(11, 15, 16, 18) #, 13, 12)
startTime = 0 # time the save file start
frameKey = 'null'
videoStart = False
cam = videoStreem.RasberryCamera()
#cam.make_video_file(str(startTime))



def startTornado():
    settings = {'speed': 100}
    app = make_app(settings)
    app.listen(2010)
    print ("OK,	  ready for co Connection")
    tornado.ioloop.IOLoop.instance().start()
	
def stopTornado():
    tornado.ioloop.IOLoop.instance().stop()
	
#help functions
def quit(): # close the program
    timestamp = datetime.now()
    GPIO.cleanup()
    print("motors diconacter")
    print(str(timestamp-startTime) + ' s. of the video')
    tornado.ioloop.IOLoop.instance().stop()

def startVideoEntry():
    global videoStart, startTime
    videoStart = True
    startTime = datetime.now()
    cam.make_video_file(str(startTime))
       
       
def storLogFile(key):
    #file_path = str(os.path.dirname(os.path.realpath(__file__))) + "/session-"+str(timestampFile)+".txt"
    file_path = '/home/pi/DataSet/session*'+str(startTime)+"*.txt"
    command = '( )'
    #timestamp = datetime.now()
    #commend code
    if '87' in key:
    	command = '0001'#"up"
    elif '65' in key:
    	command = '0010'#"left"
    elif '68' in key:
    	command = '0100'#"right"
    elif '83' in key:
    	command =  '1000'#"down"
    else:
        command = 'null'
    log_entry = str(command)# + "*" + str(timestamp-startTime)
    with open(file_path, "a") as writer:
    	writer.write(log_entry + "\n")

def make_app(settings):
	return tornado.web.Application([
		(r"/drive", MultipleKeysHandler), (r"/post", PostHandler, {'settings': settings}),
                (r"/makeData", MultipleKeysHandlerForDataSet),
		(r'/video_feed', StreamHandler),
		(r'/setparams', SetParamsHandler),
		(r'/(?:image)/(.*)', tornado.web.StaticFileHandler, {'path': './image'}),
		(r'/(?:css)/(.*)', tornado.web.StaticFileHandler, {'path': './css'}),
		(r'/(?:js)/(.*)', tornado.web.StaticFileHandler, {'path': './js'})
	])	
		
		
		
# the start of the web server classe's

class PostHandler(tornado.web.RequestHandler):
    # I don't understand decorators, but this fixed my "can't set attribute" error
    @property
    def settings(self):
        return self._settings

    @settings.setter
    def settings(self, settings):
        self._settings = settings

    def initialize(self, settings):
        self.settings = settings

    def post(self):
        timestamp = datetime.now()

        data_json = tornado.escape.json_decode(self.request.body)

        allowed_commands = set(['65', '87', '68', '83', '27', '13'])

        command = data_json['command']
        command = list(command.keys())

        command = set(command)
        command = allowed_commands & command
	
	'''for key in allowed_commands:
            if key in command:
                storLogFile(command)
                #print(command)'''
        global frameKey
        frameKey = command
        
        speed = 100
        if '65' in command:
            motor.forward_left(speed)
        elif '87' in command:
            motor.forward(speed)
        elif '68' in command:
            motor.forward_right(speed)
        elif '83' in command:
            motor.backward(100)
        elif '27' in command:
            quit()
        elif '13' in command:
            startVideoEntry()
        else:
            motor.stop()


class MultipleKeysHandler(tornado.web.RequestHandler):
    def get(self):
	try:
            self.render("www/stream.html")
        except IOError as e:
            self.write("404: Not Found")
			
			#MultipleKeysHandlerForDataSet
class MultipleKeysHandlerForDataSet(tornado.web.RequestHandler):
    def get(self):
	try:
            self.render("www/saveVideo.html")
			#call a save video funcan.
        except IOError as e:
            self.write("404: Not Found")
			

class SetParamsHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def post(self):
       
        width = int(self.get_argument('width'))
        height = int(self.get_argument('height'))
        # try to change resolution
        try:
            cam.set_resolution(width, height)
            self.write({'resp': 'ok'})
        except:
            self.write({'resp': 'bad'})
            self.flush()
            self.finish()


class StreamHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        """
        functionality: generates GET response with mjpeg stream
        input: None
        :return: yields mjpeg stream with http header
        """
        # Set http header fields
        self.set_header('Cache-Control',
                         'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0')
        self.set_header('Connection', 'close')
        self.set_header('Content-Type', 'multipart/x-mixed-replace;boundary=--boundarydonotcross')
	#cam = videoStreem.RasberryCamera()
        while True:
            # Generating images for mjpeg stream and wraps them into http resp
            global frameKey
            if videoStart:
                storLogFile(frameKey)
                print (frameKey)
                #frameKey = 'null'
                img = cam.getSave_frame()
            else:
                img = cam.get_frame()
            self.write("--boundarydonotcross\n")
            self.write("Content-type: image/jpeg\r\n")
            self.write("Content-length: %s\r\n\r\n" % len(img))
            self.write(str(img))
            yield tornado.gen.Task(self.flush)
