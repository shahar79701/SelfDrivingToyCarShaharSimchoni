import tensorflow as tf
import numpy as np
import makeData

#lode data
train_x, train_y, test_x, test_y = makeData.getData()
print (str(len(test_y[0])))
print (str(len(test_y)))

'''
train_x = makeData.dataPreper(train_x)
train_y = makeData.dataPreperLabel(train_y)
test_x = makeData.dataPreper(test_x)
test_y = makeData.dataPreperLabel(test_y)
'''

print (str(len(train_y[0])))
print (str(len(train_y)))


n_nodes_hl1 = 67

n_classes = 4
batch_size = 5

picSize = 33280 # enter pic size
x = tf.placeholder('float', [None, picSize])
y = tf.placeholder('float', [None, 4])

hidden_1_layer = {'weights': tf.Variable(tf.random_normal([picSize, n_nodes_hl1])),
                  'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))}

output_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_classes])),
                'biases': tf.Variable(tf.random_normal([n_classes])), }


def neural_network_model(data):
   
    l1 = tf.add(tf.matmul(data, hidden_1_layer['weights']), hidden_1_layer['biases'])
    l1 = tf.nn.relu(l1)

    output = tf.matmul(l1, output_layer['weights']) + output_layer['biases']

    return output

saver = tf.train.Saver()

def train_neural_network(x):
    prediction = neural_network_model(x)
    
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    hm_epochs = 1
    with tf.Session() as sess:

        sess.run(tf.global_variables_initializer())
        
        for epoch in range(hm_epochs):
            epoch_loss = 0
            i = 0
            while  i < len(train_x): # size of training da
                start = i
                end = i+batch_size
                
                
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])

                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                epoch_loss = epoch_loss + c

                i = i + batch_size
	    #saver.save(sess,"model.ckpt")
            print('Epoch', epoch+1, 'completed out of', hm_epochs, 'loss:', epoch_loss)
        
        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))

        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        
        
        test_xe = np.array(test_x)
        test_ye = np.array(test_y)
        print(test_ye)
        print(batch_y)
        print('Accuracy:', accuracy.eval({x: test_xe, y: test_ye}))


train_neural_network(x)