#!/usr/bin/env python
import cv2
import numpy as np
#from datetime import datetime
from picamera.array import PiRGBArray
from picamera import PiCamera



class RasberryCamera(object):

    """ Init camera """
    def __init__(self):

	self.cam = PiCamera()
        self.w = 208
	self.h = 160
	self.cam.resolution = (self.w,self.h)
	self.cam.framerate = 20
	self.rawCapture = PiRGBArray(self.cam, size=(self.w,self.h))
	
	
	self.video = 0 # the video file
	self.videoF = 0 # the video file
	#fourcc = cv2.cv.CV_FOURCC(*'XVID')
	#self.video = cv2.VideoWriter('/home/pi/video1.avi',fourcc,5,(self.w,self.h))
	
	
    def make_video_file(self,time):
	#timestamp = datetime.now()
	#str(timestamp)
	fourcc = cv2.cv.CV_FOURCC(*'XVID')
	self.video = cv2.VideoWriter('/home/pi/DataSet/drive*'+time+'*.avi',fourcc,20,(self.w,self.h))
	self.videoF = cv2.VideoWriter('/home/pi/DataSet/driveFILTER*'+time+'*.avi',fourcc,20,(self.w,self.h))	

	
    def getSave_frame(self):
        self.cam.capture(self.rawCapture, 'bgr', use_video_port=True)	
        image = self.rawCapture.array
	self.rawCapture.seek(0)
	self.rawCapture.truncate()
		#start of filter
	lower_red = np.array([30,150,50])
	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
		
	lower_red = np.array([30,150,50])
	upper_red = np.array([255,255,180])
		
	mask = cv2.inRange(hsv, lower_red, upper_red)
	res = cv2.bitwise_and(image,image, mask= mask)
		#end filter
	self.video.write(image)
        self.videoF.write(res)
        
        #cv2.imshow('res',mask)

        
	if image.size == 0:
	    image = np.zeros((self.h, self.w, 3), np.uint8)
	    cv2.putText(image, 'Error T_T \ X_X \ Y_Y', (40, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)
	    
	ret, jpeg = cv2.imencode('.jpg', image)	
	return jpeg.tostring()
    
    def get_frame(self):
	self.cam.capture(self.rawCapture, 'bgr', use_video_port=True)	
        image = self.rawCapture.array
	self.rawCapture.seek(0)
	self.rawCapture.truncate()
	
        if image.size == 0:
            image = np.zeros((self.h, self.w, 3), np.uint8)
            cv2.putText(image, 'No camera', (40, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)
        
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tostring()

		